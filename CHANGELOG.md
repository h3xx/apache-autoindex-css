# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed

- Build with bootstrap 5.2.3, sass 1.58.3.
- Don't use a VERSION file.
- Do use an AUTHORS file.

### [1.3.1]

### Changed

- Build no longer requires installing all of Angular.
- Build no longer requires `sassc`; now uses the Sass compiler that comes from
  the `sass` Node library.

### Fixed

- Fix CHANGELOG missing from release tarballs.

## [1.3.0] - 2023-01-18

### Changed

- Rename project to apache-modern-autoindex.

## [1.2.2] - 2023-01-02

### Fixed
- Text now wraps normally in the Size column.

## [1.2.1] - 2022-12-31

### Fixed
- Fix long text wrapping; long filenames no longer affect the width of the
  table (#1).

## [1.2.0] - 2022-12-27

### Changed
- Refactored color usage to use `var()`. Results in ~20% smaller compiled file size.

## [1.1.0] - 2022-12-27

### Added
- Add optional footer with link back to project

### Fixed
- Minor improvements to deploy process
- Highlight name/icon headers if one is hovered

## [1.0.0] - 2022-07-29
Initial published version
