# Release checklist

- Check AUTHORS (use 'git shortlog -nse' to check)
- Add version number on top of CHANGELOG.md
- Update version in package.json
