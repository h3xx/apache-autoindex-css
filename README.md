# Apache Modern Autoindex

This project is a modern restyling of the default file listing tables provided
by Apache's `mod_autoindex` module that ships with all major distributions of
Apache.

A mod*ern* autoindex if you will. ;-)

## Features

- Automatic dark mode support
- Mobile/tablet/small screen support using responsive design breakpoints; hides
  a column on small screens
- User interface improvements:
  - Clicking anywhere in the table cell activates links

| Light Mode | Dark Mode
| - | -
| ![Light mode screenshot](../flair/light-mode.png) | ![Dark mode screenshot](../flair/dark-mode.png)

## Building

Run `make` to generate `autoindex.css`.

Build requirements:

- `nodejs`
- `pnpm`

## Installation

Compile the css and then install it as `/var/www/apache-css/autoindex.css`.

Then, update your Apache configuration:

```apache
IndexOptions \
    Charset=UTF-8 \
    DescriptionWidth=* \
    FancyIndexing \
    FoldersFirst \
    HTMLTable \
    IconsAreLinks \
    NameWidth=* \
    VersionSort \
    XHTML \

# Optional: Include footer
ReadmeName /apache-css/FOOTER.html

# Re-route incoming requests to this folder
Alias /apache-css "/var/www/apache-css"

<Directory "/var/www/apache-css">
    # Required - allow requests
    Require all granted
    # Optional - allow indexing the folder
    Options Indexes
    # Optional - only if you're keeping a git clone in the folder
    AllowOverride all
</Directory>

# Make the responsive design aspects of the stylesheet work on mobile devices
IndexHeadInsert '\
    <meta name="viewport" content="width=device-width, initial-scale=1"/>\
'
IndexStyleSheet "/apache-css/autoindex.css"
```
