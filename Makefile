SASS = pnpx sass
SASS_ARGS = \
			--no-charset \
			--no-source-map \
			--no-unicode \
			--style=compressed \

PNPM = pnpm
PNPM_INSTALL = $(PNPM) install

MAKE_RELEASE = bin/make-release.sh

TARGETS = autoindex.css

autoindex.css: src/autoindex.scss $(wildcard src/*.scss)

$(wildcard src/_bootstrap-*.scss): pnpm

%.css:
	$(SASS) $(SASS_ARGS) $<:$@

all: $(TARGETS)

pnpm: pnpm-lock.yaml
	$(PNPM_INSTALL)

release: $(TARGETS) $(MAKE_RELEASE)
	$(MAKE_RELEASE)

clean:
	$(RM) $(TARGETS)

.PHONY: all clean \
	pnpm \
	release
