#!/bin/bash

# Build a release tarball for apache-css
#
# Copyright (C) 2022 Dan Church.
# License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
# with Commons Clause 1.0 (https://commonsclause.com/).
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.
# You may NOT use this software for commercial purposes.

shopt -s dotglob

USAGE() {
    printf 'Usage: %s [VERSION]\n' \
        "${0##*/}"
}

check_required_binaries() {
    local BIN MISSING=()
    for BIN; do
        if ! type -t "$BIN" &>/dev/null; then
            MISSING+=("$BIN")
        fi
    done
    if [[ ${#MISSING[@]} -gt 0 ]]; then
        printf 'Error: You are missing required programs:\n' >&2
        for BIN in "${MISSING[@]}"; do
            printf -- '- %s\n' "$BIN" >&2
        done
        exit 2
    fi
}

check_required_binaries \
    mkdir \
    realpath \
    tar

WORKDIR=${0%/*}
BASEDIR=$(realpath -- "$WORKDIR/..")
cd "$BASEDIR" || exit

RELEASESDIR=$BASEDIR/releases

PROJECT=apache-modern-autoindex
VERSION=$1
if [[ -z $VERSION ]]; then
    if [[ -e .git ]]; then
        check_required_binaries git
        VERSION=$(git describe --tags)
    fi
fi
if [[ -z $VERSION ]]; then
    USAGE >&2
    exit 1
fi
TAR=$RELEASESDIR/$PROJECT-$VERSION.tar.xz

INCLUDE=(
    *.css
    .htaccess
    *.html
    AUTHORS*
    CHANGELOG*
    LICENSE*
    package.json
    pnpm-lock.*
    README*
    src/*
)

printf 'Creating "%s"\n' \
    "$TAR"

mkdir -p -- "$RELEASESDIR" || exit
tar -cJ \
    -f "$TAR" \
    --dereference \
    -- \
    "${INCLUDE[@]}"
